from allianceauth.project_template.project_name.settings.base import *
import os

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(os.path.join(PROJECT_DIR, 'tests.sqlite3')),
    },
}

ROOT_URLCONF = 'tests.urls'

INSTALLED_APPS += [
    'mattermost',
]

WSGI_APPLICATION = 'tests.wsgi.application'

LOGGING['loggers']['mattermost'] = {'handlers':['log_file', 'console'],'level':'DEBUG'}

CELERY_ALWAYS_EAGER = True

MATTERMOST_TOKEN = os.environ.get('MATTERMOST_TOKEN', None)
MATTERMOST_URL = os.environ.get('MATTERMOST_URL', 'http://localhost:8065/')
