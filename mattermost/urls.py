from django.urls import path, include
from . import views

app_name = 'mattermost'

module_urls = [
    path('activate/', views.CreateMattermostAccountView.as_view(), name='activate'),
    path('deactivate/', views.DeactivateMattermostAccountView.as_view(), name='deactivate'),
    path('reset/', views.ResetMattermostAccountView.as_view(), name='reset')
]

urlpatterns = [
    path('mattermost/', include((module_urls, app_name), namespace=app_name)),
]
