from django.contrib import admin, messages
from django import forms
from allianceauth.services.admin import ServicesUserAdmin
from .models import MattermostUser, MattermostGroupMapping, ResourceAlreadyExistsError
from .api import get_team, get_all_teams, DuplicateUsernameError, DuplicateEmailError, InvalidOrMissingParameters
from .tasks import update_team_accounts, update_names, verify_team_membership
from .settings import TASK_PRIORITY


class MattermostUserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, min_length=5, max_length=64)

    class Meta:
        model = MattermostUser
        fields = ['user', 'password']

    def save(self, commit=True):
        try:
            if not commit:
                self.save_m2m = self._save_m2m
            return self.Meta.model.add_user(self.cleaned_data['user'], self.cleaned_data['password'])
        except (DuplicateUsernameError, ResourceAlreadyExistsError):
            raise forms.ValidationError(
                {'user': 'A Mattermost user already exists on the server with the generated username.'}, code='invalid')
        except DuplicateEmailError:
            raise forms.ValidationError(
                {'user': "A Mattermost user already exists on the server with this user's email."}, code='invalid')
        except InvalidOrMissingParameters:
            raise forms.ValidationError('General API failure: invalid parameters.', code='invalid')


def deactivate(admin, request, queryset):
    failed_pks = []
    for account in queryset:
        try:
            account.deactivate()
        except:
            failed_pks += [account.pk]
    if failed_pks:
        messages.error(request, "Failed to deactivate {} mattermost users: {}".format(len(failed_pks),
            ', '.join(a.user.username for a in queryset.filter(pk__in=failed_pks))), extra_tags='error')
    if queryset.count() - len(failed_pks) > 0:
        messages.success(request, "Deactivated {} mattermost users.".format(queryset.count() - len(failed_pks)))


deactivate.short_description = 'Deactivate selected mattermost users'


def reactivate(admin, request, queryset):
    failed_pks = []
    for account in queryset:
        try:
            account.reactivate()
        except:
            failed_pks += [account.pk]
    if failed_pks:
        messages.error(request, "Failed to reactivate {} Mattermost users: {}".format(len(failed_pks),
            ', '.join(a.user.name for a in queryset.filter(pk__in=failed_pks))), extra_tags='error')
    if queryset.count() - len(failed_pks) > 0:
        messages.success(request, "Reactivated {} Mattermost users.".format(queryset.count() - len(failed_pks)))


reactivate.short_description = 'Reactivate selected mattermost users'


def check_names(admin, request, queryset, force=False):
    for pk in queryset.values_list('pk', flat=True):
        update_names.apply_async(args=[pk], kwargs={'force':force}, priority=TASK_PRIORITY)
    messages.success(request, "Queued account name update tasks for {} Mattermost users.".format(queryset.count()))


check_names.short_description = 'Check display names for selected mattermost users'


def force_update_names(admin, request, queryset):
    return check_names(admin, request, queryset, force=True)


force_update_names.short_description = 'Force update display names for selected mattermost users'


@admin.register(MattermostUser)
class MattermostUserAdmin(ServicesUserAdmin):
    list_display = ServicesUserAdmin.list_display + ('username', 'active')
    list_filter = ServicesUserAdmin.list_filter + ('active',)

    actions = [deactivate, reactivate, check_names, force_update_names]

    def has_change_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return 'username', 'display_name', 'guid'
        return ()

    def get_form(self, request, obj=None, **kwargs):
        if not obj or not obj.pk:
            return MattermostUserForm
        return super().get_form(request, obj=obj, **kwargs)


def get_team_choices():
    teams = get_all_teams()
    return [(t['id'], t['name']) for t in teams]


class MattermostGroupMappingForm(forms.ModelForm):
    guid = forms.ChoiceField(choices=get_team_choices, label='Name')

    class Meta:
        model = MattermostGroupMapping
        fields = ["guid"]

    def clean(self):
        super().clean()
        team_uuid = self.cleaned_data["guid"]
        if team_uuid:
            name = get_team(self.cleaned_data["guid"])['display_name']
            if MattermostGroupMapping.objects.filter(name=name).exists():
                raise forms.ValidationError(
                    "A Mattermost group mapping with this team's name already exists.")
            self.instance.name = name
        return self.cleaned_data


def check_team_accounts(admin, request, queryset):
    for team in queryset:
        update_team_accounts.apply_async(args=[team.pk], priority=TASK_PRIORITY)
    messages.success(request, "Queued account update tasks for {} mattermost group mappings.".format(queryset.count()))


check_team_accounts.short_description = 'Check membership of selected mattermost group mappings'


def update_team_name(admin, request, queryset):
    failed_pks = []
    for team in queryset:
        try:
            team.name = get_team(team.guid)['display_name']
            team.save()
        except:
            failed_pks += [team.pk]
    if failed_pks:
        messages.error(request, "Failed to update names for {} Mattermost teams: {}.".format(len(failed_pks),
            ', '.join(t.name for t in queryset.filter(pk__in=failed_pks))), extra_tags='error')
    if queryset.count() - len(failed_pks) > 0:
        messages.success(request, "Updated names for {} mattermost teams.".format(queryset.count() - len(failed_pks)))


update_team_name.short_description = 'Update team names for selected mattermost group mappings'


def check_team_membership(admin, request, queryset):
    for pk in queryset.values_list('pk', flat=True):
        verify_team_membership.apply_async(args=[pk], priority=TASK_PRIORITY)
    messages.success(request, "Queued team membership verification tasks for {} mattermost group mappings.".format(queryset.count()))

check_team_membership.short_description = 'Verify server membership for selected mattermost group mappings'


@admin.register(MattermostGroupMapping)
class MattermostGroupMappingAdmin(admin.ModelAdmin):
    actions = [update_team_name, check_team_accounts, check_team_membership]
    list_display = ['name', 'number_of_accounts']
    filter_horizontal = ['users', 'groups', 'states']

    def get_fields(self, request, obj=None):
        if obj and obj.pk:
            return 'name', 'guid', 'states', 'groups', 'users'
        return 'guid',

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.pk:
            return 'name', 'guid'

    def get_form(self, request, obj=None, **kwargs):
        if not obj or not obj.pk:
            return MattermostGroupMappingForm
        return super().get_form(request, obj=obj, **kwargs)
