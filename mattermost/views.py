from django.contrib import messages
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _
from allianceauth.services.abstract import BaseCreatePasswordServiceAccountView, BaseDeactivateServiceAccountView, \
    BaseResetPasswordServiceAccountView
from .models import MattermostUser, ResourceAlreadyExistsError
from .api import DuplicateEmailError, DuplicateUsernameError
import logging

logger = logging.getLogger(__name__)


class MattermostViewMixin:
    service_name = 'mattermost'
    permission_required = 'mattermost.access_mattermost'
    model = MattermostUser


class CreateMattermostAccountView(MattermostViewMixin, BaseCreatePasswordServiceAccountView):
    def get(self, request):
        try:
            svc_obj = MattermostUser.add_user(request.user)
            return render(request, self.template_name,
                context={'credentials': svc_obj.credentials, 'service': self.service_name, 'view': self})

        except ResourceAlreadyExistsError:
            messages.error(request, _("That service account already exists"))
            return redirect(self.index_redirect)
        except DuplicateEmailError:
            messages.error(request,
                'A {} account with your email already exists. Contact your admin.'.format(self.service_name))
            return redirect(self.index_redirect)
        except DuplicateUsernameError:
            messages.error(request,
                'Failed to find a suitable username for your {} account. Contact your admin.'.format(self.service_name))
            return redirect(self.index_redirect)


class DeactivateMattermostAccountView(MattermostViewMixin, BaseDeactivateServiceAccountView):
    # we don't actually delete accounts on the server because :reasons:
    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.deactivate()
        return redirect(self.index_redirect)


class ResetMattermostAccountView(MattermostViewMixin, BaseResetPasswordServiceAccountView):
    pass
