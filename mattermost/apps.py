from django.apps import AppConfig


class MattermostServiceConfig(AppConfig):
    name = 'mattermost'
    label = 'mattermost'
