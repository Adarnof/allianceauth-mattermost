from mattermostdriver.driver import Driver
from mattermostdriver.exceptions import InvalidOrMissingParameters, ResourceNotFound
from urllib.parse import urlparse
from .settings import URL, TOKEN
import logging

# see https://github.com/Vaelor/python-mattermost-driver/issues/68
logging.getLogger('mattermostdriver.websocket').disabled=True


class DeferredDriver(Driver):
    # prevent login when debug unless a resource is called. Should allow testing.
    _logged_in = False
    _api = {}

    def __getattribute__(self, item):
        if not item.startswith('_') and item in self._api and not self._logged_in:
            _ = self.login()
        return super().__getattribute__(item)

    def login(self):
        try:
            # need to set logged_in to True to avoid recursion issue with Driver.login() calling self.users
            self._logged_in = True
            return super().login()
        except:
            # if failed to login, then reset to try again
            self._logged_in = False
            raise

url = urlparse(URL)

# from what I can tell, mattermost doesn't care how many instances are connected, nor for how long
options = {'url': url.hostname, 'scheme': url.scheme, 'port': url.port, 'token': TOKEN}
if url.path and url.path != '/':
    # for some reason the driver fails when basepath is included and set to '' or '/'
    options.update({'basepath': url.path})
d = DeferredDriver(options=options)


class DuplicateEmailError(InvalidOrMissingParameters):
    pass


class DuplicateUsernameError(InvalidOrMissingParameters):
    pass


# re-implement mattermostdriver methods to clean up the attribute mapping and prevent logging in of driver until needed

def get_user(user_id):
    """
    :param user_id: GUID of user
    :return: dict of user details
    """
    return d.users.get_user(user_id)

def get_user_by_username(username):
    """
    :param username: username
    :return: dict of user details
    """
    return d.users.get_user_by_username(str(username))


def create_team(name):
    """
    :param name: team name
    :return: dict of team details
    """
    return d.teams.create_team(params={'name': str(name), 'display_name': str(name), 'type': 'I'})


def add_user_to_team(team_id, user_id):
    """
    Doesn't fail even if already in team.
    :param team_id: GUID of team
    :param user_id: GUID of user
    :return: dict of team/user relationship
    """
    return d.teams.add_user_to_team(team_id, options={'team_id': team_id, 'user_id': user_id})


def remove_user_from_team(team_id, user_id):
    """
    :param team_id: GUID of team
    :param user_id: GUID of user
    :return: dict of {'status': 'OK'} if worked
    """
    return d.teams.remove_user_from_team(team_id, user_id)


def create_user(username, display_name, email, password):
    """
    :param username: account username to login with
    :param display_name: formatted name to display
    :param email: account email
    :param password: account password
    :return: dict of user details
    """
    try:
        return d.users.create_user(
            options={'username': str(username), 'first_name': str(display_name), 'last_name': '', 'email': email,
                     'password': password})
    except InvalidOrMissingParameters as e:
        if 'username' in str(e):
            raise DuplicateUsernameError(e)
        if 'email' in str(e):
            raise DuplicateEmailError(e)
        raise


def deactivate_user(user_id):
    """
    :param user_id: GUID of user
    :return: dict of {'status': 'OK'} if worked
    """
    return d.users.deactivate_user(user_id)


def reactivate_user(user_id):
    """
    :param user_id: GUID of user
    :return: dict of {'status': 'OK'} if it worked
    """
    return d.users.update_user_active_status(user_id, options={'active': True})


def set_user_password(user_id, password):
    """
    :param user_id: GUID of user
    :param password: new password
    :return: dict of {'status': 'OK'} if it worked
    """
    return d.users.update_user_password(user_id, options={'new_password': password})


def update_name(user_id, username, display_name):
    """
    :param user_id: GUID of user
    :param username: desired account username
    :param display_name: desired formatted name to display
    :return: dict of user details
    """
    try:
        return d.users.patch_user(user_id, options={'username': str(username), 'first_name': str(display_name), 'last_name': ''})
    except InvalidOrMissingParameters as e:
        if 'username' in str(e):
            raise DuplicateUsernameError(e)
        raise


def get_user_teams(user_id):
    """
    :param user_id: GUID of user
    :return: list of dicts of team details
    """
    return d.teams.get_user_teams(user_id)


def add_multiple_users_to_team(team_id, user_ids):
    """
    :param team_id: GUID of team
    :param user_ids: list of GUIDs of users
    :return: list of dicts of team/user relationships
    """
    users = []
    for u in user_ids:
        users += [{'team_id': team_id, 'user_id': u}]
    return d.teams.add_multiple_users_to_team(team_id, options=users)

def get_all_teams():
    """
    :return: list of dicts of teams
    """
    return d.teams.get_teams()

def get_team_by_name(name):
    """
    :param name: team name
    :return: dict of team details
    """
    return d.teams.get_team_by_name(name)

def get_team(team_id):
    """
    :param team_id: GUID of team
    :return: dict of team details
    """
    return d.teams.get_team(team_id)

def get_team_members(team_id):
    """
    :param team_id: GUID of team
    :return: set of user GUIDs
    """
    members = set()
    page = 0
    results = d.teams.get_team_members(team_id, params={'page': page, 'per_page': 100})
    while results and page < 100:  # sanity, strange errors could trap us here forever
        members |= set([m['user_id'] for m in results])
        page += 1
        results = d.teams.get_team_members(team_id, params={'page': page, 'per_page': 100})
    return members
