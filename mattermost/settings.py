from django.conf import settings

URL = getattr(settings, 'MATTERMOST_URL', 'http://localhost:8065')
TOKEN = getattr(settings, 'MATTERMOST_TOKEN', '')
DEFAULT_TEAM = getattr(settings, 'MATTERMOST_DEFAULT_TEAM', '')
STATUS_OK = getattr(settings, 'MATTERMOST_STATUS_OK', 'OK')
TASK_PRIORITY = getattr(settings, 'MATTERMOST_TASK_PRIORITY', 3)
