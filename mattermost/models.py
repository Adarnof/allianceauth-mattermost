from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from allianceauth.services.hooks import NameFormatter
from allianceauth.authentication.models import State
from .settings import DEFAULT_TEAM, STATUS_OK, TASK_PRIORITY
from . import api
from collections import OrderedDict
import logging
import random
import string
import re

User = get_user_model()

logger = logging.getLogger(__name__)


class ResourceAlreadyExistsError(Exception):
    pass


class IteratedUsername:
    def __init__(self, fullname: str, max_length: int = 22, iterations: int = None):
        self.fullname = re.sub("[^\w.]", '', fullname).lower()
        self.max_length = max_length
        if not iterations:
            iterations = 0
            match = re.search('_\d+$', self.fullname)
            if match:
                # username appears to already have been iterated, so separate name and iteration counter
                self.fullname = self.fullname[:match.start()]
                iterations = int(re.search('\d+', match.group()).group())
        self.iterations = iterations

    def __str__(self):
        if self.iterations > 0:
            append_length = len(str(self.iterations)) + 1
            return f'{self.fullname[:self.max_length-append_length]}_{self.iterations}'
        return self.fullname[:self.max_length]

    __repr__ = __str__

    def iterate(self):
        self.iterations += 1

    def __eq__(self, other):
        # turn other into an iterated username and set same parameters as this one
        other_name = self.__class__(str(other), max_length=self.max_length)
        # determine who has been iterated more, for truncation considerations
        max_iterations = max(self.iterations, other_name.iterations)
        _iterations = self.iterations
        other_name.iterations = max_iterations
        self.iterations = max_iterations
        # see if, when iterations match, the string is equivalent
        result = str(self) == str(other_name)
        self.iterations = _iterations
        return result


def random_password():
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(16)])


def determine_name(user):
    from .auth_hooks import MattermostService
    return NameFormatter(MattermostService(), user).format_name()


class MattermostUser(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE, related_name='mattermost')
    guid = models.CharField(unique=True, max_length=26)
    username = models.CharField(max_length=22, unique=True)  # username name max
    display_name = models.CharField(max_length=55)  # character name max 37 plus 15 for two max-length tickers and space
    active = models.BooleanField(default=True)

    class Meta:
        permissions = (('access_mattermost', 'Can access mattermost service'),)
        default_permissions = ('add', 'view', 'delete')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.credentials = OrderedDict()

    def __str__(self):
        return self.username

    def update_names(self):
        display_name = determine_name(self.user)
        username = IteratedUsername(self.user.profile.main_character.character_name or self.user.username)
        i = 0
        while i < 10:
            try:
                logger.debug(
                    f"Trying to update mattermost names for user {self.user} to username {username}, display name {display_name}")
                remote = api.update_name(self.guid, username, display_name)
                if remote:
                    self.display_name = remote['first_name']
                    self.username = remote['username']
                    self.save()
                    self.credentials.update({'username': username})
                    logger.debug(
                        f"Updated mattermost names for user {self.user} to username {username}, display name {display_name}")
                    return True
            except api.DuplicateUsernameError:
                # username collision. That's unfortunate.
                i += 1
                username.iterate()
        else:
            logger.warning(
                f'Unable to update mattermost account name for user {self.user}: failed to generate acceptable username after {i} iterations.')
            return False

    def deactivate(self, force=False):
        if self.active or force:
            logger.debug(f"Trying to deactivate mattermost account for user {self.user}")
            if api.deactivate_user(self.guid)['status'] == STATUS_OK:
                self.active = False
                self.save()
                logger.info(f"Deactivated mattermost account for user {self.user}")
                return True
        return False

    def reactivate(self, force=False):
        if not self.active or force:
            logger.debug(f"Trying to reactivate mattermost account for user {self.user}")
            if api.reactivate_user(self.guid)['status'] == STATUS_OK:
                self.active = True
                self.save()
                logger.info(f"Reactivated mattermost account for user {self.user}")
                return True
        return False

    def set_password(self, password):
        logger.debug(f"Trying to update mattermost password for user {self.user}")
        if api.set_user_password(self.guid, password)['status'] == STATUS_OK:
            self.credentials.update({'password': password})
            logger.info(f"Updated mattermost password for user {self.user}")
            return True
        return False

    def reset_password(self):
        self.update_names()
        self.update_teams()
        return self.set_password(random_password())

    def add_to_team(self, team_uuid):
        logger.debug(f"Trying to add user {self.user} to mattermost team {team_uuid}")
        return api.add_user_to_team(self.guid, team_uuid)

    @classmethod
    def __add_user(cls, user: User, password: str):
        try:
            username = IteratedUsername(user.profile.main_character.character_name)
        except AttributeError:  # no main character
            username = IteratedUsername(user.username)
        display_name = determine_name(user)
        try:
            logger.debug(f'Trying to create mattermost user with username {username}, email {user.email}')
            remote = api.create_user(username, display_name, user.email, password)
            logger.info(f'Created mattermost account for user {user}')
            obj = cls.objects.create(user=user, username=remote['username'],
                display_name=f"{remote['first_name']} {remote['last_name'].strip()}", guid=remote['id'], active=True)
            obj.credentials.update({'username': obj.username, 'password': password})
            return obj
        except api.DuplicateUsernameError:
            # do we know them?
            logger.debug(f'Duplicate username encountered when adding mattermost user {user} with username {username}')
            remote = api.get_user_by_username(username)
            if not cls.objects.filter(guid=remote['id']).exists() and remote['username'] == username and remote[
                'email'] == user.email:
                # we've found an orphaned account. How odd. Let's reattach.
                logger.warning(f'Found orphaned mattermost account {username}. Reattaching to user {user}')
                obj = cls.objects.create(user=user, username=remote['username'], display_name=remote['first_name'],
                    guid=remote['id'])
                # force all sessions to expire for safety
                obj.deactivate()
                obj.reactivate()
                obj.set_password(password)
                obj.credentials.update({'username': obj.username, 'password': password})
                return obj
            else:
                # username collision. That's unfortunate.
                i = 0
                while i < 10:  # sanity. There's no way we'd get this many collisions. And if we do, it's too long.
                    username.iterate()
                    try:
                        logger.debug(f'Trying to create mattermost user with username {username}, email {user.email}')
                        remote = api.create_user(username, display_name, user.email, password)
                        logger.info(f'Created mattermost account through username iteration for user {user}')
                        obj = cls.objects.create(user=user, username=remote['username'],
                            display_name=remote['first_name'], guid=remote['id'])
                        obj.credentials.update({'username': obj.username, 'password': password})
                        return obj
                    except api.DuplicateUsernameError:
                        # welp. try again.
                        i += 1
                else:
                    # how did we fail??? did the iterator generate something too long?
                    logger.debug(
                        f'Failed to generate acceptable mattermost username from {username} after {i} iterations.')
                    raise

    @classmethod
    def add_user(cls, user: User, password: str = ''):
        password = password or random_password()
        if cls.objects.filter(user=user).exists():
            obj = cls.objects.get(user=user)
            if obj.active:
                raise ResourceAlreadyExistsError()
            else:
                # time to reactivate the account
                obj.reactivate()
                obj.set_password(password)
                obj.update_names()
                return obj
        else:
            obj = cls.__add_user(user, password=password)
            if DEFAULT_TEAM:
                obj = cls.objects.get(user=user)
                obj.add_to_team(DEFAULT_TEAM)
            return obj

    def update_teams(self):
        available_teams = MattermostGroupMapping.objects.filter(available_teams_query(self))
        remove_teams = self.mattermost_teams.difference(available_teams)
        add_teams = available_teams.difference(self.mattermost_teams.all())

        if add_teams or remove_teams:
            logger.debug(
                f"Updating mattermost teams for user {self.user}. Adding {add_teams.count()}, removing {remove_teams.count()}")
            for t in remove_teams:
                t.remove_account(self)
            for t in add_teams:
                t.add_account(self)
        return True


class MattermostGroupMapping(models.Model):
    name = models.CharField(max_length=50, unique=True)
    guid = models.CharField(unique=True, max_length=26)
    groups = models.ManyToManyField(Group, related_name='mattermost_teams', blank=True)
    states = models.ManyToManyField(State, related_name='mattermost_teams', blank=True)
    users = models.ManyToManyField(User, related_name='mattermost_teams', blank=True)
    accounts = models.ManyToManyField(MattermostUser, related_name='mattermost_teams', blank=True, editable=False)

    def __str__(self):
        return self.name

    def number_of_accounts(self):
        return self.accounts.count()

    def add_account(self, account: MattermostUser):
        logger.debug(f"Trying to add mattermost user {account.user} to team {self.name}")
        if api.add_user_to_team(self.guid, account.guid):
            self.accounts.add(account)
            logger.debug(f"Added mattermost user {account.user} to team {self.name}")
            return True
        return False

    def add_multiple_accounts(self, accounts: models.QuerySet):
        logger.debug(f"Trying to add {accounts.count()} accounts to mattermost team {self.name}")
        api.add_multiple_users_to_team(self.guid, [u.guid for u in accounts])
        logger.debug(f"Added {accounts.count()} accounts to mattermost team {self.name}")
        self.accounts.add(*accounts)
        return True

    def remove_account(self, account: MattermostUser):
        try:
            logger.debug(f"Trying to remove mattermost user {account.user} from team {self.name}")
            if api.remove_user_from_team(self.guid, account.guid)['status'] == STATUS_OK:
                self.accounts.remove(account)
                logger.debug(f"Removed mattermost user {account.user} from team {self.name}")
                return True
            return False
        except api.InvalidOrMissingParameters:
            # user is already not a part of this team. Weird, right? Let's make sure.
            teams = api.get_user_teams(account.guid)
            for t in teams:
                if t['team_id'] == self.guid:
                    logger.warning(f'Failed to remove mattermost user {account.user} from team {self.name}')
                    raise
            else:
                # user is definitely not part of this team. Safe to remove.
                logger.debug(f'Mattermost user {account.user} unexpectedly removed from team {self.name}')
                self.accounts.remove(account)
                return True

    def update_accounts(self):
        available_accounts = MattermostUser.objects.filter(available_accounts_query(self))
        remove_accounts = self.accounts.difference(available_accounts)
        add_accounts = available_accounts.difference(self.accounts.all())

        if add_accounts or remove_accounts:
            logger.debug(
                f"Updating accounts for mattermost team {self.name}. Adding {add_accounts.count()}, removing {remove_accounts.count()}")
            if add_accounts:
                self.add_multiple_accounts(add_accounts)
            for account in remove_accounts:
                self.remove_account(account)  # no bulk remove endpoint :((((
        return True

    def audit_accounts(self):
        members = api.get_team_members(self.guid)
        accounts = set(self.accounts.values_list('guid', flat=True))
        add_users = accounts - members
        remove_users = members - accounts

        if add_users or remove_users:
            logger.warning(
                f'Incorrect membership for Mattermost team {self.name}. Adding {len(add_users)}, removing {len(remove_users)}.')
        if add_users:
            _ = [api.add_user_to_team(self.guid, user_id) for user_id in add_users]
        if remove_users:
            _ = [api.remove_user_from_team(self.guid, user_id) for user_id in remove_users]

    @classmethod
    def create_team(cls, name):
        try:
            logger.debug(f'Trying to create mattermost team {name}')
            remote = api.create_team(name)
            return cls.objects.create(name=remote['display_name'], guid=remote['id'])
        except api.InvalidOrMissingParameters as e:
            # team name already exists? did we forget about this one? spooky.
            try:
                t = api.get_team_by_name(name)
                if not cls.objects.filter(name=name).exists():
                    logger.debug(f'Mattermost team {name} already exists on server. Creating model.')
                    return cls.objects.create(name=name, guid=t['id'])
                else:
                    raise ResourceAlreadyExistsError()
            except api.ResourceNotFound:
                # Schrodinger's team. Exists but doesn't exist. Something is broken.
                raise e


def check_team_accounts_on_update(sender, instance, action, reverse, model, pk_set, *args, **kwargs):
    from .tasks import update_team_accounts
    if action.startswith('post_'):
        # the change has been committed to the database
        if reverse:
            # this is from one of (User, Group, State).mattermost_teams
            for pk in pk_set:
                update_team_accounts.apply_async(args=[pk], property=TASK_PRIORITY)
        else:
            update_team_accounts.apply_async(args=[instance.pk], property=TASK_PRIORITY)
    elif action == 'pre_clear' and reverse:
        # this is a pre_clear using the reverse side. Don't do this. There's no way to tell what changed once committed.
        for team in instance.mattermost_teams.all():
            # 10 second delay is probably enough, right?
            update_team_accounts.apply_async(args=[team.pk], property=TASK_PRIORITY, countdown=10)


# let's be gentle on the api, and only check the team once per emission by using a uid
# see https://docs.djangoproject.com/en/3.2/topics/signals/#preventing-duplicate-signals
models.signals.m2m_changed.connect(check_team_accounts_on_update, sender=MattermostGroupMapping.users.through,
    dispatch_uid='mattermost.UserGroupMapping.users')
models.signals.m2m_changed.connect(check_team_accounts_on_update, sender=MattermostGroupMapping.groups.through,
    dispatch_uid='mattermost.UserGroupMapping.groups')
models.signals.m2m_changed.connect(check_team_accounts_on_update, sender=MattermostGroupMapping.states.through,
    dispatch_uid='mattermost.UserGroupMapping.states')


# nested querysets have bad performance on MySQL backends
# for safety, force evaluation to get pks, and then use that list for filtering
# see https://docs.djangoproject.com/en/3.2/ref/models/querysets/#in

def available_teams_query(account: MattermostUser):
    query = models.Q(users=account.user)
    query |= models.Q(groups__in=list(account.user.groups.all().values_list('pk', flat=True)))
    query |= models.Q(states=account.user.profile.state)
    return query


def available_accounts_query(team: MattermostGroupMapping):
    query = models.Q(user__in=list(team.users.all().values_list('pk', flat=True)))
    query |= models.Q(user__groups__in=list(team.groups.all().values_list('pk', flat=True)))
    query |= models.Q(user__profile__state__in=list(team.states.all().values_list('pk', flat=True)))
    return query
