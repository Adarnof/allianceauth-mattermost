from allianceauth.services.tasks import shared_task, QueueOnce
from .settings import TASK_PRIORITY
from .models import MattermostUser, MattermostGroupMapping, IteratedUsername, determine_name
import logging

logger = logging.getLogger(__name__)


@shared_task(bind=True, name='mattermost.update_names', base=QueueOnce)
def update_names(self, user_pk: int, force: bool=False):
    try:
        account = MattermostUser.objects.get(user__pk=user_pk)
        if account.active:
            username = IteratedUsername(account.user.profile.main_character.character_name or account.user.username)
            if force or username != IteratedUsername(account.username) or determine_name(account.user) != account.display_name:
                logger.debug(f"Triggering mattermost username update for user {account.user}")
                account.update_names()
    except MattermostUser.DoesNotExist:
        pass


@shared_task(bind=True, name='mattermost.update_teams', base=QueueOnce)
def update_teams(self, user_pk: int):
    try:
        account = MattermostUser.objects.get(user__pk=user_pk)
        if account.active:
            account.update_teams()
    except MattermostUser.DoesNotExist:
        pass


@shared_task(bind=True, name='mattermost.update_team_accounts', base=QueueOnce)
def update_team_accounts(self, team_pk: int):
    try:
        team = MattermostGroupMapping.objects.get(pk=team_pk)
        team.update_accounts()
    except MattermostGroupMapping.DoesNotExist:
        pass


@shared_task(bind=True, name='mattermost.verify_team_membership', base=QueueOnce)
def verify_team_membership(self, team_pk: int):
    try:
        team = MattermostGroupMapping.objects.get(pk=team_pk)
        team.audit_accounts()
    except MattermostGroupMapping.DoesNotExist:
        pass


@shared_task(bind=True, name='mattermost.update_all_account_names', base=QueueOnce)
def update_all_account_names(self):
    for pk in MattermostUser.objects.filter(active=True).values_list('user_id', flat=True):
        update_names.apply_async(args=[pk], priority=TASK_PRIORITY)


@shared_task(bind=True, name='mattermost.update_all_team_accounts', base=QueueOnce)
def update_all_team_accounts(self):
    for pk in MattermostGroupMapping.objects.values_list('pk', flat=True):
        update_team_accounts.apply_async(pk=pk, priority=TASK_PRIORITY)


@shared_task(bind=True, name='mattermost.verify_all_team_membership', base=QueueOnce)
def verify_all_team_membership(self):
    for pk in MattermostGroupMapping.objects.values_list('pk', flat=True):
        verify_team_membership.apply_async(args=[pk])
