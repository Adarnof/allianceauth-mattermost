from django.template.loader import render_to_string
from allianceauth.services.hooks import ServicesHook
from allianceauth.hooks import register
from .urls import urlpatterns
from .settings import URL, TASK_PRIORITY
from .models import MattermostUser
import logging

logger = logging.getLogger(__name__)


class MattermostService(ServicesHook):
    def __init__(self):
        super(MattermostService, self).__init__()
        self.urlpatterns = urlpatterns
        self.service_url = URL
        self.name = 'mattermost'
        self.access_perm = 'mattermost.access_mattermost'
        self.name_format = '[{corp_ticker}] {character_name}'
        self.account_model = MattermostUser

    def render_services_ctrl(self, request):
        urls = self.Urls()
        urls.auth_activate = 'mattermost:activate'
        urls.auth_deactivate = 'mattermost:deactivate'
        urls.auth_reset_password = 'mattermost:reset'

        try:
            username = MattermostUser.objects.get(user=request.user, active=True).username
        except MattermostUser.DoesNotExist:
            username = ''

        return render_to_string(
            self.service_ctrl_template,
            {
                'service_name': self.title,
                'urls': urls,
                'service_url': self.service_url,
                'username': username,
            },
            request=request
        )

    def delete_user(self, user, notify_user=False):
        return user.mattermost.deactivate()

    def sync_nickname(self, user):
        try:
            if user.mattermost.active:
                from .tasks import update_names
                update_names.apply_async(args=[user.pk], priority=TASK_PRIORITY)
        except MattermostUser.DoesNotExist:
            pass

    def validate_user(self, user):
        if not user.has_perm(self.access_perm):
            try:
                logger.debug("User {} no longer has access permission. Deactivating mattermost account.".format(user))
                user.mattermost.deactivate()
            except MattermostUser.DoesNotExist:
                pass

    def service_active_for_user(self, user):
        return user.has_perm(self.access_perm)

    def get_all_accounts(self):
        return self.account_model.objects.filter(active=True)


@register('services_hook')
def register_service():
    return MattermostService()
