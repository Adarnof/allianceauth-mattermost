# allianceauth-mattermost

A service module for [Alliance Auth](https://gitlab.com/allianceauth/allianceauth/) that enables management of a [Mattermost](https://mattermost.com/) server.

## Installation
### 1. Install and configure Mattermost server
Follow the [online documentation](https://docs.mattermost.com/guides/deployment.html#install-guides) for installation of a Mattermost server.

Create an admin account, and in the Server Console, enable the use of [personal access tokens](https://developers.mattermost.com/integrate/admin-guide/admin-personal-access-token/) by going to `Integrations` - `Bot Accounts` and setting `Enable Bot Account Creation` to `true`.

The default team member limit is 50. If you expect to have more than 50 users, increase this by going to `Site Configuration` - `Users and Teams` and setting `Max Users Per Team`. Bigger is better. Errors relating to full teams are not handled by this app.

The server can be customized by going to `Site Configuration` - `Customization`.

### 2. Create default team and retrieve bot token

When user accounts are added to Mattermost, by default they are not in any teams. If you wish to have a default team to which new accounts are added, return to the System Console by clicking on the grid in the top left and selecting `System Console`. Under `User Management` - `Teams` click on the team you wish to use. The end of the URL in your browser is the team's GUID which you will need for later.

Note that accounts are only ever added to this team when users activate the Mattermost service in Alliance Auth. Members are never removed automatically. An alternative to this is to create a public team, which any account can join at any time.

Bot tokens are created through the account settings menu, which is only available when accessing a team. Once you've created a team, open your account settings menu through the icon in the top right, and select `Account Settings`. Under `Security` click on `Personal Access Tokens` and `Create Token`. Enter a useful description such as `allianceauth` and save. A warning about system admin permissions is expected. You'll need the `Access Token` field for later.


### 3. Install Mattermost service in Alliance Auth

In your Alliance Auth `settings/local.py` file, add `'mattermost',` to the list of `INSTALLED_APPS` like so:

    INSTALLED_APPS += [
        ...
        mattermost,
    ]

Additionally, add new settings:

    MATTERMOST_URL = 'https://mattermost.example.com'
    MATTERMOST_BOT_TOKEN = 'Access Token you retrieved goes here'
    MATTERMOST_DEFAULT_TEAM = 'team GUID here, if desired'

Note the singe quotes around the values.

Once the settings have been entered, run the migrations with `python manage.py migrate`.

To allow users to access this service, grant the `mattermost | mattermost user | can access mattermost service` permission to users, groups, or states through the Alliance Auth admin site.

### 4. Configure group mappings

Mattermost doesn't have "groups" like other services. Instead, this app uses teams to simulate the beheviour of groups. To this end, you need to define mappings which instruct this app as to which groups go to which teams.

In your Alliance Auth admin site, navigate to `Mattermost` - `Mattermost group mappings`. When you select `Add`, you will be presented with a list of team names from your Mattermost server. Select one, and save. Note that you can only have one group mapping per team.

When you edit a group mapping, you are presented with 3 ways to define members: users, groups, and states. Any of these added to the group mapping will results in users, group members, or users of a given state with Mattermost service accounts to be added to the selected team. Likewise removal of any of these will result in those users having their Mattermost account removed from the selected team.

### 5. Usage

Users who have the `mattermost | mattermost user | Can access mattermost service` permission will see the Mattermost service as an option on the Services page of Alliance Auth.

Activating an account through the services page will return a username and password for authenticating on the Mattermost server. Display names on the server can be adjusted by specifying a [Service Name Format](https://allianceauth.readthedocs.io/en/latest/features/services/nameformats.html) , default is `[{corp_ticker}] {character_name}`.

Both username and display name on the Mattermost server are updated when a user's main character changes, or when their main character's affiliations change. Check the Services page for an updated username.

Resetting the password through the Services page yields a new password, updated username/display name, and performs a check on team membership.

Deleting the account through the Services page results in deactivation of the Mattermost user account.

## Administration

### Admin site actions

Additional actions are available through the admin site. Deletion of any Mattermost users or Mattermost group mappings will result in those becoming unmanaged by Alliance Auth.

#### Mattermost users

This list displays all accounts managed by Alliance Auth on the Mattermost server. Mattermost does not delete accounts, instead marking them as "inactive" which disables log-in. You can see the active status, and a filter exists on the right-hand side to sort by it.

To manually deactivate or reactivate a user account, select the checkbox next to the account(s), choose one of `De/Reactivate selected mattermost users` from the `Action` drop-down box, and click on `Go`.

Deactivation of accounts should be used in place of deletion. Deleting a Mattermost user will prevent Alliance Auth from managing it.

To verify account usernames and display names, select the checkbox next to the account(s), choose `Check display names for selected mattermost users` from the `Action` drop-down box, and click on `Go`. This task will only update account names where the expected username or display name differs from what is stored in the Mattermost user model.

To force update usernames and display names, select the checkbox next to the account(s), choose `Force update display names for selected mattermost users` from the `Action` drop-down box, and click on `Go`.

#### Mattermost group mappings

This list displays all group mappings for teams managed by Alliance Auth on the Mattermost server.

Team names are unique, but can be changed on the Mattermost server. To update names of Mattermost group mappings, select the checkbox next to the team(s), choose `Update team names for selected mattermost group mappings` from the `Action` drop-down box, and click on `Go`.

If problems are encountered with team membership on the Mattermost server, two options exist:
 - a manual re-check can be initiated by selecting the checkbox next to the team(s), choosing `Check membership of selected mattermost group mappings` from the `Action` drop-down box, and clicking on `Go`. This will re-run the logic that decides which Mattermost users are impacted by the selected Mattermost group mappings.
 - if the above doesn't correct the issue, or if Mattermost users not managed by Alliance Auth are in the selected teams, the state of the Mattermost group mapping can be re-synchronized with the Mattermost server by selecting the checkbox next to the desired team(s), choosing `Verify server membership for selected mattermost group mappings` from the `Action` drop-down box, and clicking on `Go`. This will ensure all Mattermost users who should be in that team are indeed members of the Mattermost server, and that no one else is present in the team.

### Periodic tasks
Some admin site actions can be configured to run periodically using celery's task scheduler:
- `mattermost.update_all_account_names`: force updates all account names on the Mattermost server
- `mattermost.update_all_team_accounts`: re-runs the team membership logic on all Mattermost group mappings
- `mattermost.verify_all_team_membership`: re-synchronizes all Mattermost group mappings with the Mattermost server, including removal of unknown accounts

These tasks can be set to run by adding an entry in your Alliance Auth `settings/local.py` file such as:

    CELERYBEAT_SCHEDULE['mattermost_update_all_account_names'] = {
        'task': 'mattermost.update_all_account_names'
        'schedule': crontab(minute=0, hour='*/1')
    }

Configuring periodic tasks can have performance implications on large servers - the package used for interacting with the Mattermost API does not handle rate limiting. The above schedule is for illustration only. For more information on scheduling tasks, see the [Celery documentation](https://docs.celeryproject.org/en/stable/userguide/periodic-tasks.html).

### Error handling

This app attempts to tolerate data integrity problems by reconnecting with "lost" accounts. Mattermost requires that emails and usernames be unique. When an Alliance Auth user creates an account, or a Mattermost user model is created through the admin site, username is determined based on the user's main character (or account username, if none). If an account already exists on the Mattermost server with that username, the following logic is executed:
- get the Mattermost user's GUID. If it is known to Alliance Auth (and thus belongs to another Alliance Auth user), iterate the username
- if the GUID is unknown, the emails are compared. If they do not match, iterate the username
- if the email matches, then the existing Mattermost account is stored as the newly created Mattermost user

In this way, if for instance a Mattermost user is deleted through the admin site, the original account holder can regain access to this Mattermost account. When this occurs, the Mattermost user is deactivated (to force logout all existing sessions), reactivated, then has its username/display name/password updated.

No logic is currently executed for email collisions where the usernames are different. This could occur when a main character changes (and thus the generated username changes) after the Mattermost user model has been deleted. In such situations, the Alliance Auth user will see an error message instructing them to contact their admin. To resolve, change the email stored in their Alliance Auth account, or change the email of the existing Mattermost account.

Username iteration is limited to 10 tries. If no suitable username is found in that time, the Alliance Auth user will see an error message instructing them to contact their admin. To resolve, change usernames of the offending accounts in the Mattermost server.
